package ua.study.security.repo;

import ua.study.security.model.User;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class UserRepo {

    List<User> users;

    public UserRepo() {
        this.users = new ArrayList<>();
    }

    public void add(User user) {
        user.setId(users.size()+1);
        users.add(user);
    }

    public User getByName(String username) {
        return users.stream()
                .filter(user -> user.getUsername().equals(username))
                .findFirst()
                .orElse(null);
    }

    public User getById (int id) {
         return users.stream()
                .filter(user -> user.getId() == id)
                .findFirst()
                 .orElse(null);
    }

}
