package ua.study.security.security.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import ua.study.security.model.LoginCredentials;
import ua.study.security.model.User;
import ua.study.security.security.service.JwtService;
import ua.study.security.security.service.LoginService;

@RestController
public class LoginController {

    private final LoginService loginService;
    private final JwtService jwtService;

    @Autowired
    public LoginController(LoginService loginService, JwtService jwtService) {
        this.loginService = loginService;
        this.jwtService = jwtService;
    }

    @PostMapping("/login")
    public String login(@RequestBody LoginCredentials loginCredentials) {
        User user = loginService.login(loginCredentials);
        if(user != null) {
            return jwtService.generateToken(user);
        } else {
            throw new SecurityException("Bad credentials");
        }
    }

    @GetMapping
    public String main() {
        return "Hello";
    }

}
