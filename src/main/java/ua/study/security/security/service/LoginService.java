package ua.study.security.security.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ua.study.security.model.LoginCredentials;
import ua.study.security.model.User;
import ua.study.security.repo.UserRepo;

@Service
public class LoginService {

    private UserRepo userRepo;

    @Autowired
    public LoginService(UserRepo userRepo){
        this.userRepo = userRepo;
    }

    public User login(LoginCredentials loginCredentials) {
        User user = userRepo.getByName(loginCredentials.getUsername());
        if(user != null && user.getPassword().equals(loginCredentials.getPassword())) {
            return user;
        } else {
            return null;
        }
    }
}
