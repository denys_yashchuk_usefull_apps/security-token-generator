package ua.study.security.security.service;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import ua.study.security.model.Role;
import ua.study.security.model.User;

import javax.annotation.PostConstruct;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.Base64;
import java.util.Date;

import static java.time.ZoneOffset.UTC;

@Service
public class JwtService {

    @Value("${security.jwt.token.secret-key}")
    private String key;

    @Value("${security.jwt.token.expire-days}")
    private Integer expirationDay;

    @PostConstruct
    protected void init() {
        key = Base64.getEncoder().encodeToString(key.getBytes());
    }

    public String generateToken(User user) {

        Claims claims = Jwts.claims();
        claims.put("sub", Integer.toString(user.getId()));
        claims.put("role", user.getRole());

        Date now = new Date();
        Date expiration = Date.from(LocalDateTime.now(UTC).plusDays(expirationDay).toInstant(UTC));

        return Jwts.builder()
                .setIssuedAt(now)
                .setExpiration(expiration)
                .setClaims(claims)
                .signWith(SignatureAlgorithm.HS512, key)
                .compact();
    }

}
