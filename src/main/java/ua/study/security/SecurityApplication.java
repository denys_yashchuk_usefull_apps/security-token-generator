package ua.study.security;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import ua.study.security.model.Role;
import ua.study.security.model.User;
import ua.study.security.repo.UserRepo;

@SpringBootApplication
public class SecurityApplication {

    public static void main(String[] args) {
        SpringApplication.run(SecurityApplication.class, args);
    }

    @Bean
    public UserRepo userRepo() {
        UserRepo userRepo = new UserRepo();
        userRepo.add(new User("test1", "password", Role.ADMIN));
        userRepo.add(new User("test2", "password", Role.ADMIN));
        userRepo.add(new User("test3", "password", Role.USER));
        userRepo.add(new User("test4", "password", Role.USER));
        return userRepo;
    }
}
